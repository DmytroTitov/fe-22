const {src, dest, series, parallel, watch, task} = require('gulp');
const sass = require('gulp-sass');
const server = require('browser-sync').create();
const minifyjs = require('gulp-js-minify');
const uglify = require('gulp-uglify');
const cleanCSS = require('gulp-clean-css');
const clean = require('gulp-clean');
const concat = require('gulp-concat');
const imagemin = require('gulp-imagemin');
const autoprefixer = require('gulp-autoprefixer');

const path = {
    src: {
        html: 'index.html',
        scss: 'src/scss/**/*.scss',
        js: 'src/js/**/*.js',
        img: 'src/img/**/*',
        fonts: 'src/fonts/**/*'
    },
    dist: {
        root: 'dist/',
        css: 'dist/css/',
        js: 'dist/js/',
        img: 'dist/img/',
        fonts: 'dist/fonts/'
    }
}

//** FUNCTIONS **//

const cleanBuild = () => (
    src(path.dist.root, {allowEmpty: true})
        .pipe(clean())
);

const buildStyles = () => (
    src(path.src.scss)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            overrideBrowserslist: ['last 2 versions'],
            cascade: true
        }))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(concat('styles.min.css'))
        .pipe(dest(path.dist.css))
        .pipe(server.stream())
)

const buildJS = () => (
    src(path.src.js)
        .pipe(uglify())
        .pipe(minifyjs())
        .pipe(concat('scripts.min.js'))
        .pipe(dest(path.dist.js))
        .pipe(server.stream())
)

const buildImg = () => (
    src(path.src.img)
        .pipe(imagemin())
        .pipe(dest(path.dist.img))
        .pipe(server.stream())
)

const buildFonts = () => (
    src(path.src.fonts)
        .pipe(dest(path.dist.fonts))
)

const devServer = () => {
    server.init({
        server: {
            baseDir: "./"
        }
    })
}

//** WATCHER **//

const watcher = () => {
    devServer();
    watch(path.src.html).on('change', server.reload);
    watch(path.src.scss, buildStyles).on('change', server.reload);
    watch(path.src.js, buildJS).on('change', server.reload);
    watch(path.src.img, buildImg).on('change', server.reload);
}

//** TASKS **//

task('build', series(
    cleanBuild,
    parallel(
        buildStyles,
        buildImg,
        buildJS,
        buildFonts,
    )
));

task('dev', watcher);

