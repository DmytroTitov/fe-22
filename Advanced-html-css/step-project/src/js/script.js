const burgerRef = document.querySelector('.burger');
const navRef = document.querySelector('.hero-nav');

burgerRef.addEventListener('click', function () {
    navRef.classList.remove('disappear');
    burgerRef.classList.toggle('burger--opened');

    if (burgerRef.classList.contains('burger--opened')) {
        navRef.classList.add('appear');
    } else {
        navRef.classList.remove('appear');
        navRef.classList.add('disappear');
    }
});
