## Вопрос 1
>Объясните своими словами, что такое AJAX и чем он полезен при разработке на Javascript.

**Ответ:**
AJAX это аббревиатура, которая означает Asynchronous Javascript and XML, а именно набор методов для работы с веб страницей с возможностью изменений веб страницы без перезагрузки и динамического обращения к серверу для получения данных. AJAX полезен при разработке, т.к. позволяет осуществлять асинхронные запросы к серверу, что ускоряет работу приложения и не блокирует основной поток выполнения кода.
