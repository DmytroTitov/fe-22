"use strict";

function getCharacters() {

    fetch('https://swapi.dev/api/films/', {
        headers: {
            'Content-type': 'application/json'
        }
    }).then(r => r.json()).then(data => data.results).then(result => {

        result.forEach(obj => {

            const {title, episode_id, opening_crawl, characters} = obj;

            const elements = {
                root: document.querySelector('#root'),
                container: document.createElement('div'),
                title: document.createElement('h4'),
                episode: document.createElement('p'),
                crawl: document.createElement('p'),
                charactersContainer: document.createElement('ul'),
            }

            elements.title.textContent = title;
            elements.episode.textContent = episode_id;
            elements.crawl.textContent = opening_crawl;

            elements.container.append(elements.title, elements.episode, elements.crawl);

            characters.forEach(character => {
                const replacedLink = character.replace('http://', 'https://')
                fetch(replacedLink, {
                    headers: {
                        'Content-type': 'application/json'
                    }
                }).then(r => r.json()).then(data => {
                    const characterEl = document.createElement('li');
                    characterEl.textContent = data.name;
                    elements.charactersContainer.append(characterEl)
                })
            })

            elements.container.append(elements.charactersContainer);
            elements.root.append(elements.container);
        })

    })

}

getCharacters()