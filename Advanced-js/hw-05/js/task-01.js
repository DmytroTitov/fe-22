"use strict";

const btnRef = document.querySelector('button');

btnRef.addEventListener('click', getPhysicalAddress);

function renderInfo ({country, countryCode, region, regionName, city, zip}) {

    const container = document.createElement('ul');
    const countryEL = document.createElement('li');
    const countryCodeEl = document.createElement('li');
    const regionEl = document.createElement('li');
    const regionNameEl = document.createElement('li');
    const cityEl = document.createElement('li');
    const zipEl = document.createElement('li');

    countryEL.textContent = country;
    countryCodeEl.textContent = countryCode;
    regionEl.textContent = region;
    regionNameEl.textContent = regionName;
    cityEl.textContent = city;
    zipEl.textContent = zip;

    container.append(countryEL, countryCodeEl, regionEl, regionNameEl, cityEl, zipEl);

    document.body.append(container);
}

function getIP () {
    return fetch('https://api.ipify.org/?format=json', {
        headers: {
            "Content-Type": 'application/json',
        }
    }).then(r => r.json()).then(data => data.ip);
}

async function getPhysicalAddress () {
    const ip = await getIP();

    const physicalAddress = await fetch(`http://ip-api.com/json/${ip}`, {
            headers: {
                "Content-Type": 'application/json',
            }
        }).then(r => r.json());

    renderInfo(physicalAddress);
}





