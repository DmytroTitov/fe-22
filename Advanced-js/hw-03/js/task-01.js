"use strict";
class Employee {
    constructor(options) {
        this._name = options.name;
        this._age = options.age;
        this._salary = options.salary;
    }

    get name() {
        return this._name;
    }

    get age() {
        return this._age;
    }

    get salary() {
        return this._salary;
    }

    set name(newName) {
        this._name = newName;
    }

    set age(newAge) {
        this._age = newAge;
    }

    set salary(newSalary) {
        this._salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor(options) {
        super(options);
        this.lang = options.lang;
    }

    get salary() {
        return this._salary * 3;
    }
}

const jsProgrammer = new Programmer({name: 'John', age: 22, salary: 10000, lang: 'Javascript'});
const pythonProgrammer = new Programmer({name: 'Eric', age: 26, salary: 4000, lang: 'Python'});
const javaProgrammer = new Programmer({name: 'Michael', age: 32, salary: 6000, lang: 'Java'});

console.log(jsProgrammer);
console.log(pythonProgrammer);
console.log(javaProgrammer);

console.log(jsProgrammer.salary);
console.log(pythonProgrammer.salary);
console.log(javaProgrammer.salary);