import React, {Component} from 'react';
import './App.scss';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

const id = [1, 2, 3, 4];

class App extends Component {

    state = {
        isOpen: false,
        isFirstBtn: null,
        isSecondBtn: null
    }

    openModalFirstBtn = () => {
        this.setState({isOpen: true, isSecondBtn: false, isFirstBtn: true});
    }

    openModalSecondBtn = () => {
        this.setState({isOpen: true, isFirstBtn: false, isSecondBtn: true});
    }

    handleOk = () => {
        this.setState({isOpen: false});
    }

    handleCancel = () => {
        this.setState({isOpen: false});
    }

    render() {
        const {isFirstBtn, isSecondBtn} = this.state;

        return (
            <div className="App">
                <div className="btns-container">
                    <Button
                        onClick={this.openModalFirstBtn}
                        backgroundColor="#feda71"
                        text="Open first modal"
                        className="open-btn"
                    />
                    <Button
                        onClick={this.openModalSecondBtn}
                        backgroundColor="#383d48"
                        text="Open second modal"
                        className="open-btn"
                    />
                </div>
                {isFirstBtn &&
                    < Modal
                        header="Do you want to delete this file?"
                        closeButton={true}
                        text="Once you delete this file, it won’t be possible to undo this action.
                    Are you sure you want to delete it?"
                        isOpen={this.state.isOpen}
                        actions={[
                                <Button key={id[0]} backgroundColor="#b3382c" onClick={this.handleOk} className="btn btn-ok" text="Ok"/>,
                                <Button key={id[1]} backgroundColor="#b3382c" onClick={this.handleCancel} className="btn btn-cancel" text="Cancel"/>
                            ]}
                        handleCancel={this.handleCancel}
                    />}
                {isSecondBtn &&
                < Modal
                        header="Do you want to add something?"
                        closeButton={true}
                        text="Some random text"
                        isOpen={this.state.isOpen}
                        actions={
                            [
                                <Button key={id[2]} backgroundColor="#b3382c" onClick={this.handleOk} className="btn btn-ok" text="Yes"/>,
                                <Button key={id[3]} backgroundColor="#b3382c" onClick={this.handleCancel} className="btn btn-cancel" text="No"/>
                            ]
                        }
                        handleCancel={this.handleCancel}
                    />
                    }
            </div>
        )
    }
}

export default App;
