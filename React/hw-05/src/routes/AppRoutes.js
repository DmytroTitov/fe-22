import React from 'react';
import CardsList from "../components/CardsList/CardsList";
import {Redirect, Route, Switch} from "react-router-dom";
import Favorites from "../pages/Favorites/Favorites";
import Cart from "../pages/Cart/Cart";
import PropTypes from "prop-types";

const AppRoutes = (props) => {
    const {addToFavorite, openModalAdd, openModalDelete, delAllFromCart, showGoodsInCart, showUserFormData} = props;

    return (
        <>
            <Switch>
                <Redirect exact from="/" to="/home"/>
                <Route exact
                       path="/home"
                       render={() => <CardsList addToFavorite={addToFavorite}
                                                openModalAdd={openModalAdd} openModalDelete={openModalDelete}
                       />}
                />
                <Route exact
                       path="/favorites"
                       render={() => <Favorites addToFavorite={addToFavorite}
                                                openModalAdd={openModalAdd} openModalDelete={openModalDelete}
                       />}
                />
                <Route exact
                       path="/cart"
                       render={() => <Cart addToFavorite={addToFavorite} openModalAdd={openModalAdd}
                                           openModalDelete={openModalDelete} delAllFromCart={delAllFromCart}
                                           showGoodsInCart={showGoodsInCart} showUserFormData={showUserFormData}
                       />}
                />

            </Switch>
        </>
    );
};

AppRoutes.propTypes = {
    addToFavorite: PropTypes.func,
    openModalAdd: PropTypes.func,
    openModalDelete: PropTypes.func,
    delAllFromCart: PropTypes.func,
    showGoodsInCart: PropTypes.func,
    showUserFormData: PropTypes.func,
}

AppRoutes.defaultProps = {
    addToFavorite: () => {
    },
    openModalAdd: () => {
    },
    openModalDelete: () => {
    },
    delAllFromCart: () => {
    },
    showGoodsInCart: () => {
    },
    showUserFormData: () => {
    },
}

export default AppRoutes;