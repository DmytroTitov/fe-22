import React from 'react';
import Card from "../../components/Card/Card";
import PropTypes from "prop-types";
import {useSelector} from "react-redux";
import {getGoods} from "../../store/selectors";
import DeliveryForm from "../../components/DeliveryForm/DeliveryForm";

const Cart = (props) => {
    const {addToFavorite, openModalAdd, openModalDelete, delAllFromCart, showGoodsInCart, showUserFormData} = props;
    const cards = useSelector(getGoods);
    return (
        <>
            <div className="cards-container container">
                {cards.map(card => {
                    if (card.inCart) {
                        return <Card key={card.code} card={card} addToFavorite={addToFavorite}
                                     openModalAdd={openModalAdd} openModalDelete={openModalDelete} closeBtn={true}/>
                    }
                    return null;
                })}

            </div>
            <DeliveryForm delAllFromCart={delAllFromCart} showGoodsInCart={showGoodsInCart} showUserFormData={showUserFormData}/>
        </>
    );
};

Cart.propTypes = {
    cards: PropTypes.array,
    addToFavorite: PropTypes.func,
    openModalAdd: PropTypes.func,
    openModalDelete: PropTypes.func,
    delAllFromCart: PropTypes.func,
    showGoodsInCart: PropTypes.func,
    showUserFormData: PropTypes.func,
}

Cart.defaultProps = {
    cards: [],
    addToFavorite: () => {
    },
    openModalAdd: () => {
    },
    openModalDelete: () => {
    },
    delAllFromCart: () => {
    },
    showGoodsInCart: () => {
    },
    showUserFormData: () => {
    },
}

export default Cart;