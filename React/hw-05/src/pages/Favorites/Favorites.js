import React from 'react';
import Card from "../../components/Card/Card";
import PropTypes from "prop-types";
import {useSelector} from "react-redux";
import {getGoods} from "../../store/selectors";

const Favorites = (props) => {
    const {addToFavorite, openModalAdd, openModalDelete} = props;
    const cards = useSelector(getGoods);

    return (
        <div className="cards-container container">
            {cards.map(card => {
                if (card.isFavorite) {
                    return <Card key={card.code} card={card} addToFavorite={addToFavorite}
                                 openModalAdd={openModalAdd} openModalDelete={openModalDelete}/>
                }
            })}
        </div>
    );
};

Favorites.propTypes = {
    cards: PropTypes.array,
    addToFavorite: PropTypes.func,
    openModalAdd: PropTypes.func,
    openModalDelete: PropTypes.func,
}

Favorites.defaultProps = {
    cards: [],
    addToFavorite: () => {},
    openModalAdd: () => {},
    openModalDelete: () => {},
}


export default Favorites;