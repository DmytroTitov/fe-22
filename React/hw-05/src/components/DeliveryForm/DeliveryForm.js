import React from 'react';
import {Form, Field, Formik} from 'formik';
import * as Yup from 'yup';

import "./DeliveryForm.scss";
import {useDispatch} from "react-redux";
import {setUserForm} from "../../store/actions";
import PropTypes from "prop-types";

const DeliveryForm = (props) => {
    const {delAllFromCart, showGoodsInCart, showUserFormData} = props;

    const dispatch = useDispatch();

    const isAnyGoodsInCart = JSON.parse(localStorage.getItem("inCart"));

    const validationFormSchema = Yup.object().shape({
        userName: Yup.string()
            .required('Required'),
        userSurname: Yup.string()
            .required('Required'),
        userAge: Yup.number()
            .positive('Your age must be positive value')
            .integer('Your age must be integer value')
            .min(18, 'Your age must be more than 18')
            .required('Required'),
        userAddress: Yup.string()
            .required('Required'),
        userPhone: Yup.number()
            .positive('Your phone number must be positive value')
            .integer('Your phone number must be integer value')
            .required('Required'),
    });

    const submitForm = (values) => {

        const {userName, userSurname, userAge, userAddress, userPhone} = values;

        dispatch(setUserForm({
            "userName": userName,
            "userSurname": userSurname,
            "userAge": userAge,
            "userAddress": userAddress,
            "userPhone": userPhone,
        }))
        showGoodsInCart()
        delAllFromCart()
        showUserFormData({userName, userSurname, userAge, userAddress, userPhone});
    }

    return (
        <>
            {isAnyGoodsInCart.length ? <Formik
                initialValues={{
                    userName: '',
                    userSurname: '',
                    userAge: '',
                    userAddress: '',
                    userPhone: '',
                }}
                onSubmit={submitForm}
                validationSchema={validationFormSchema}
            >
                {(formikProps) => {
                    const {values, touched, errors, dirty, isValid} = formikProps;
                    return (
                        <section className="modal-container">
                            <Form className="modal-form">
                                <Field
                                    className="modal-form__input"
                                    name="userName"
                                    type="text"
                                    placeholder="Enter your name"
                                    value={values.userName}
                                />
                                {errors.userName && touched.userName &&
                                <span className='modal-form__error'>{errors.userName}</span>}
                                <Field
                                    className="modal-form__input"
                                    name="userSurname"
                                    type="text"
                                    placeholder="Enter your last name"
                                    value={values.userSurname}
                                />
                                {errors.userSurname && touched.userSurname &&
                                <span className='modal-form__error'>{errors.userSurname}</span>}
                                <Field
                                    className="modal-form__input"
                                    name="userAge"
                                    type="number"
                                    placeholder="Enter your age"
                                    value={values.userAge}
                                />
                                {errors.userAge && touched.userAge &&
                                <span className='modal-form__error'>{errors.userAge}</span>}
                                <Field
                                    className="modal-form__input"
                                    name="userAddress"
                                    type="text"
                                    placeholder="Enter your delivery address"
                                    value={values.userAddress}
                                />
                                {errors.userAddress && touched.userAddress &&
                                <span className='modal-form__error'>{errors.userAddress}</span>}
                                <Field
                                    className="modal-form__input"
                                    name="userPhone"
                                    type="tel"
                                    placeholder="Enter your phone"
                                />
                                {errors.userPhone && touched.userPhone &&
                                <span className='modal-form__error'>{errors.userPhone}</span>}
                                <button
                                    className="modal-form__submit"
                                    type="submit"
                                    disabled={!(dirty && isValid)}>
                                    Checkout
                                </button>
                            </Form>
                        </section>
                    )
                }}
            </Formik> : null
            }
        </>
    );
};

DeliveryForm.propTypes = {
    delAllFromCart: PropTypes.func,
    showGoodsInCart: PropTypes.func,
    showUserFormData: PropTypes.func,
}

DeliveryForm.defaultProps = {
    delAllFromCart: () => {
    },
    showGoodsInCart: () => {
    },
    showUserFormData: () => {
    },
}


export default DeliveryForm;