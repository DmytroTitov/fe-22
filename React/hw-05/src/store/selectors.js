export const getGoods = state => state.goods;
export const getIsLoaded = state => state.isLoaded;
export const getIsOpen = state => state.isOpen;
export const getCodeOfGoodForModal = state => state.codeOfGoodForModal;
export const getIsGoodAdd = state => state.isGoodAdd;
export const getUserForm = state => state.userForm;


