import {
    SET_CODE_OF_GOOD_FOR_MODAL,
    SET_GOOD_ADD,
    SET_GOODS,
    SET_LOADED,
    SET_OPEN,
    SET_USER_FORM
} from "./types";

const initialStore = {
    goods: [],
    isLoaded: false,
    isOpen: false,
    codeOfGoodForModal: 0,
    isGoodAdd: false,
    userForm: {},
}

const reducer = (state = initialStore, action) => {
    switch (action.type) {
        case SET_GOODS:
            return {...state, goods: action.payload}
        case SET_LOADED:
            return {...state, isLoaded: action.payload}
        case SET_OPEN:
            return {...state, isOpen: action.payload}
        case SET_CODE_OF_GOOD_FOR_MODAL:
            return {...state, codeOfGoodForModal: action.payload}
        case SET_GOOD_ADD:
            return {...state, isGoodAdd: action.payload}
        case SET_USER_FORM:
            return {...state, userForm: action.payload}
        default:
            return state;
    }
}

export default reducer;