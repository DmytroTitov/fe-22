import React from 'react';
import * as Icons from '../../themes/icons'
import PropTypes from "prop-types";

const Icon = (props) => {
    const {type, color, className} = props
    const IconJSX = Icons[type]

    if (!IconJSX) return null

    return (
        <span>
      {IconJSX({
          color: color,
          className: className
      })}
    </span>
    );
}

Icon.propTypes = {
    type: PropTypes.string,
    color: PropTypes.string,
    className: PropTypes.string,
}

Icon.defaultProps = {
    type: "star",
    color: "white",
    className: "icon",
}

export default Icon;
