import React from 'react';
import PropTypes from 'prop-types';
import './Button.scss'

const Button = (props) => {
    const {backgroundColor, text, onClick, className} = props;
    const btnStyle = {
        backgroundColor,
    }

    return (
        <>
            <button className={className} onClick={onClick} style={btnStyle}>{text}</button>
        </>
    );
}

Button.propTypes = {
    backgroundColor: PropTypes.string,
    text: PropTypes.node,
    onClick: PropTypes.func,
    className: PropTypes.string,
}

Button.defaultProps = {
    backgroundColor: "transparent",
    text: "Button text",
    onClick: () => {
    },
    className: "btn"
}

export default Button;