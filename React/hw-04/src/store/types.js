export const SET_GOODS = "SET_GOODS";
export const SET_LOADED = "SET_LOADED";
export const SET_OPEN = "SET_OPEN";
export const SET_CODE_OF_GOOD_FOR_MODAL = "SET_CODE_OF_GOOD_FOR_MODAL";
export const SET_GOOD_ADD = "SET_GOOD_ADD";
