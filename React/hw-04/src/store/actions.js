import {SET_CODE_OF_GOOD_FOR_MODAL, SET_GOOD_ADD, SET_GOODS, SET_LOADED, SET_OPEN} from "./types";

export const setGoods = (data) => {
    return {type: SET_GOODS, payload: data}
}

export const setLoaded = (data) => {
    return {type: SET_LOADED, payload: data}
}

export const setOpen = (data) => {
    return {type: SET_OPEN, payload: data}
}

export const setCodeOfGoodForModal = (data) => {
    return {type: SET_CODE_OF_GOOD_FOR_MODAL, payload: data}
}

export const setGoodAdd = (data) => {
    return {type: SET_GOOD_ADD, payload: data}
}