import {setGoods, setLoaded} from "./actions";

const normalizedData = data => {
    return data.map(el => {
        const favorites = JSON.parse(localStorage.getItem("favorites")) || [];
        const inCart = JSON.parse(localStorage.getItem("inCart")) || [];
        el.isFavorite = favorites.includes(el.code);
        el.inCart = inCart.some(elem => elem.id === el.code);
        return el;
    })
}

export const getGoodsFromServer = (dispatch) => {
    fetch("goods.json").then(res => res.json()).then(data => {
        const normalizedGoods = normalizedData(data);
        dispatch(setGoods(normalizedGoods));
        dispatch(setLoaded(true));
    })
}

