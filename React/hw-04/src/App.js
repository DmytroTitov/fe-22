import React, {useEffect} from 'react';
import './App.scss';
import Modal from "./components/Modal/Modal";
import Button from "./components/Button/Button";
import Nav from "./components/Nav/Nav";
import AppRoutes from "./routes/AppRoutes";
import {useDispatch, useSelector} from 'react-redux';
import {getCodeOfGoodForModal, getGoods, getIsGoodAdd, getIsLoaded} from "./store/selectors";
import {setCodeOfGoodForModal, setGoodAdd, setGoods, setOpen} from "./store/actions";
import {getGoodsFromServer} from "./store/operations";

const App = () => {

    const goods = useSelector(getGoods);
    const isLoaded = useSelector(getIsLoaded);
    const codeOfGoodForModal = useSelector(getCodeOfGoodForModal);
    const isGoodAdd = useSelector(getIsGoodAdd);

    const dispatch = useDispatch();

    useEffect(() => {
        getGoodsFromServer(dispatch);
    }, [])

    const setToLocalStorage = (code, storageName) => {
        let arr = JSON.parse(localStorage.getItem(storageName)) || [];

        if (storageName === "inCart") {
            if (arr.some(el => el.id === code)) {
                arr.forEach(el => {
                        if (el.id === code) {
                            el.count = el.count + 1
                        }
                    }
                )
            } else {
                arr.push({'id': code, 'count': 1})
            }
            const inCart = JSON.stringify(arr);
            localStorage.setItem(storageName, inCart);
        } else {
            arr = (arr.includes(code) ? arr.filter(el => el !== code) : arr.concat(code));
        }
        const favorites = JSON.stringify(arr);
        localStorage.setItem(storageName, favorites);
    }

    const deleteFromLocalStorage = (code, storageName) => {
        let arr = JSON.parse(localStorage.getItem(storageName)) || [];

        if (storageName === "inCart") {
            arr = arr.filter(el => el.id !== code);
            const inCart = JSON.stringify(arr);
            localStorage.setItem(storageName, inCart);
        }
    }

    const addToFavorite = code => {

        const newArr = goods.map(el => {
            if (el.code === code) {
                el.isFavorite = !el.isFavorite
            }
            return el;
        })
        setToLocalStorage(code, "favorites");
        dispatch(setGoods(newArr));
    }

    const addToCart = code => {

        const newArr = goods.map(el => {
            if (el.code === code) {
                el.inCart = true;
            }
            return el;
        })
        setToLocalStorage(code, "inCart");
        dispatch(setGoods(newArr));
    }

    const delFromCart = (code) => {
        const newArr = goods.map(el => {
            if (el.code === code) {
                el.inCart = false;
            }
            return el;
        })
        dispatch(setGoods(newArr));
        deleteFromLocalStorage(code, "inCart");
    }

    const openModalAdd = (code) => {
        dispatch(setOpen(true));
        dispatch(setGoodAdd(true));
        dispatch(setCodeOfGoodForModal(code));
    }

    const openModalDelete = (code) => {
        dispatch(setOpen(true));
        dispatch(setGoodAdd(false));
        dispatch(setCodeOfGoodForModal(code));
    }

    const handleOkAdd = (code) => {
        dispatch(setOpen(false));
        dispatch(setGoodAdd(false));
        addToCart(code);
    }

    const handleOkDelete = (code) => {
        dispatch(setOpen(false));
        dispatch(setGoodAdd(false));
        delFromCart(code);
    }

    const handleCancel = () => {
        dispatch(setOpen(false));
        dispatch(setGoodAdd(false));
    }

    if (!isLoaded) {
        return <div>...Loading</div>;
    }

    return (
        <div className="App">
            <Nav/>
            <AppRoutes
                       addToFavorite={addToFavorite}
                       openModalAdd={openModalAdd}
                       openModalDelete={openModalDelete}
            />
            {isGoodAdd &&
            <Modal
                header="Do you want to add to the cart?"
                closeButton={true}
                text="Please click ok button to add this good into the cart"
                actions={
                    [
                        <Button key={1} backgroundColor="#b3382c" onClick={() => handleOkAdd(codeOfGoodForModal)}
                                className="btn btn-ok" text="Ok"/>,
                        <Button key={2} backgroundColor="#b3382c" onClick={handleCancel} className="btn btn-cancel"
                                text="Cancel"/>
                    ]
                }
                handleCancel={handleCancel}
            />
            }

            {!isGoodAdd &&
            <Modal
                header="Do you want to delete?"
                closeButton={true}
                text="Please click ok button to delete this good from the cart"
                actions={
                    [
                        <Button key={1} backgroundColor="#b3382c" onClick={() => handleOkDelete(codeOfGoodForModal)}
                                className="btn btn-ok" text="Ok"/>,
                        <Button key={2} backgroundColor="#b3382c" onClick={handleCancel} className="btn btn-cancel"
                                text="Cancel"/>
                    ]
                }
                handleCancel={handleCancel}
            />
            }
        </div>
    )
}

export default App;
