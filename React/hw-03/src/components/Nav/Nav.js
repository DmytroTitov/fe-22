import React from 'react';
import {NavLink} from "react-router-dom";
import "./Nav.scss"

const Nav = () => {
    return (
        <nav className="menu">
            <ul className="menu__list">
                <li className="menu__item">
                    <NavLink to="/home" className="menu__link" activeClassName="menu__item--current">Home</NavLink>
                </li>
                <li className="menu__item">
                    <NavLink to="/favorites" className="menu__link" activeClassName="menu__item--current">Favorites</NavLink>
                </li>
                <li className="menu__item">
                    <NavLink to="/cart" className="menu__link" activeClassName="menu__item--current">Cart</NavLink>
                </li>
            </ul>
        </nav>
    );
};

export default Nav;