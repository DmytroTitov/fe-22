import React from 'react';
import Icon from "../Icon/Icon";
import './Card.scss';
import PropTypes from "prop-types";
import Button from "../Button/Button";

const Card = (props) => {

    const {
        card: {name, price, url, code, color, isFavorite, inCart},
        addToFavorite,
        openModalAdd,
        openModalDelete,
        closeBtn
    } = props;
    const crossBtnText = <span className="close-btn__close-icon">X</span>;

    return (
        <>
            <div className="product-wrap">
                <div className="product-image">
                    {closeBtn &&
                    <Button onClick={() => openModalDelete(code)} className="close-btn" text={crossBtnText} backgroundColor={"#fff"}/>
                    }
                    <a href="#"><img alt={name} src={url}/></a>
                    <div className="shadow"></div>
                    <div className="actions">
                        <div className="actions-btn">
                            <div className="add-to-cart">
                                <a className="add-to-cart-button"
                                   href="#"
                                   title="Add to cart"
                                   onClick={() => openModalAdd(code)}
                                >
                                    <Icon
                                        type="cart"
                                        color={inCart ? "gold" : "white"}
                                        className="cart-icon"
                                    />

                                </a>
                            </div>
                            <div className="add-to-links">
                                <div className="add-to-wishlist">
                                    <a className="add-to-wishlist-button"
                                       href="#"
                                       title="Add to favorites"
                                       onClick={() => addToFavorite(code)}
                                    >
                                        <Icon
                                            type="star"
                                            color={isFavorite ? "gold" : "white"}
                                            className="cart-icon"
                                        />
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="product-list">
                    <h3 className="product-list__item">{name}</h3>
                    <div className="product-list__item">&#36; {price}</div>
                    <div className="product-list__item">Vendor code: {code}</div>
                    <div className="product-list__item">Color: {color}</div>
                </div>
            </div>
        </>
    );
}

Card.propTypes = {
    name: PropTypes.string,
    price: PropTypes.number,
    url: PropTypes.string,
    code: PropTypes.number,
    color: PropTypes.string,
    isFavorite: PropTypes.bool,
    inCart: PropTypes.bool,
    addToFavorite: PropTypes.func,
    openModalAdd: PropTypes.func,
    openModalDelete: PropTypes.func,
    closeBtn: PropTypes.bool
}

Card.defaultProps = {
    name: "Fruit name",
    price: 0,
    url: "",
    code: 0,
    color: "Fruit color",
    isFavorite: false,
    inCart: false,
    addToFavorite: () => {
    },
    openModalAdd: () => {
    },
    openModalDelete: () => {
    },
    closeBtn: false
}

export default Card;