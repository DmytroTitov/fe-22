import React from 'react';
import Button from "../Button/Button";
import PropTypes from 'prop-types';
import './Modal.scss'

const Modal = (props) => {

    const {header, closeButton, text, actions, isOpen, handleCancel} = props;
    const crossBtnText = <span className="modal-btn__close-icon">X</span>;
    return (
        <>
            {isOpen &&
            <div className="modal-wrap">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-header__title">{header}</h4>
                            {closeButton &&
                            <Button onClick={handleCancel} className="modal-btn" text={crossBtnText}/>
                            }
                        </div>
                        <div className="modal-body">
                            <p className="modal-body__text">{text}</p>
                        </div>
                        <div className="modal-footer">
                            {actions.map(el => el)}
                        </div>
                    </div>
                </div>
                <div onClick={handleCancel} className="modal"></div>
            </div>
            }
        </>
    );
}

Modal.propTypes = {
    header: PropTypes.string,
    closeButton: PropTypes.bool,
    text: PropTypes.string,
    actions: PropTypes.array,
    isOpen: PropTypes.bool,
    handleCancel: PropTypes.func
}

Modal.defaultProps = {
    header: "Modal header",
    closeButton: true,
    text: "Modal text",
    actions: [],
    isOpen: false,
    handleCancel: () => {
    }
}

export default Modal;