import React from 'react';
import Card from "../Card/Card";
import "./CardsList.scss"
import PropTypes from "prop-types";

const CardsList = (props) => {

    const {cards, addToFavorite, openModalAdd, openModalDelete} = props;
    return (
        <div className="cards-container container">
            {cards.map(card => {
                return <Card key={card.code} card={card} addToFavorite={addToFavorite}
                             openModalAdd={openModalAdd} openModalDelete={openModalDelete}/>
            })}
        </div>
    );
}

CardsList.propTypes = {
    cards: PropTypes.array,
    addToFavorite: PropTypes.func,
    openModalAdd: PropTypes.func,
    openModalDelete: PropTypes.func,

}

CardsList.defaultProps = {
    cards: [],
    addToFavorite: () => {},
    openModalAdd: () => {},
    openModalDelete: () => {},
}

export default CardsList;