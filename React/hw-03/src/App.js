import React, {useEffect, useState} from 'react';
import './App.scss';
import Modal from "./components/Modal/Modal";
import Button from "./components/Button/Button";
import Nav from "./components/Nav/Nav";
import AppRoutes from "./routes/AppRoutes";

const App = () => {

    const [goods, setGoods] = useState([]);
    const [isLoaded, setLoaded] = useState(false);
    const [isOpen, setOpen] = useState(false);
    const [codeOfGoodForModal, setCodeOfGoodForModal] = useState(0);
    const [isGoodAdd, setGoodAdd] = useState(false);

    const normalizedData = data => {
        return data.map(el => {
            const favorites = JSON.parse(localStorage.getItem("favorites")) || [];
            const inCart = JSON.parse(localStorage.getItem("inCart")) || [];
            el.isFavorite = favorites.includes(el.code);
            el.inCart = inCart.some(elem => elem.id === el.code);
            return el;
        })
    }

    useEffect(() => {
        fetch("goods.json").then(res => res.json()).then(data => {
            const normalizedGoods = normalizedData(data);
            setGoods(normalizedGoods);
            setLoaded(true);
        })
    }, [])

    const setToLocalStorage = (code, storageName) => {
        let arr = JSON.parse(localStorage.getItem(storageName)) || [];

        if (storageName === "inCart") {
            if (arr.some(el => el.id === code)) {
                arr.forEach(el => {
                        if (el.id === code) {
                            el.count = el.count + 1
                        }
                    }
                )
            } else {
                arr.push({'id': code, 'count': 1})
            }
            const inCart = JSON.stringify(arr);
            localStorage.setItem(storageName, inCart);
        } else {
            arr = (arr.includes(code) ? arr.filter(el => el !== code) : arr.concat(code));
        }
        const favorites = JSON.stringify(arr);
        localStorage.setItem(storageName, favorites);
    }

    const deleteFromLocalStorage = (code, storageName) => {
        let arr = JSON.parse(localStorage.getItem(storageName)) || [];

        if (storageName === "inCart") {
            arr = arr.filter(el => el.id !== code);
            const inCart = JSON.stringify(arr);
            localStorage.setItem(storageName, inCart);
        }
    }

    const addToFavorite = code => {

        const newArr = goods.map(el => {
            if (el.code === code) {
                el.isFavorite = !el.isFavorite
            }
            return el;
        })
        setToLocalStorage(code, "favorites");
        setGoods(newArr);
    }

    const addToCart = code => {

        const newArr = goods.map(el => {
            if (el.code === code) {
                el.inCart = true;
            }
            return el;
        })
        setToLocalStorage(code, "inCart");
        setGoods(newArr);
    }

    const delFromCart = (code) => {
        const newArr = goods.map(el => {
            if (el.code === code) {
                el.inCart = false;
            }
            return el;
        })
        setGoods(newArr);
        deleteFromLocalStorage(code, "inCart");
    }

    const openModalAdd = (code) => {
        setOpen(true);
        setGoodAdd(true);
        setCodeOfGoodForModal(code);
    }

    const openModalDelete = (code) => {
        setOpen(true);
        setGoodAdd(false);
        setCodeOfGoodForModal(code);
    }

    const handleOkAdd = (code) => {
        setOpen(false);
        setGoodAdd(false);
        addToCart(code);
    }

    const handleOkDelete = (code) => {
        setOpen(false);
        setGoodAdd(false);
        delFromCart(code);
    }

    const handleCancel = () => {
        setOpen(false);
        setGoodAdd(false);
    }

    if (!isLoaded) {
        return <div>...Loading</div>;
    }

    return (
        <div className="App">
            <Nav/>
            <AppRoutes isOpen={isOpen}
                       addToFavorite={addToFavorite}
                       openModalAdd={openModalAdd}
                       openModalDelete={openModalDelete}
                       goods={goods}
            />
            {isGoodAdd &&
            <Modal
                header="Do you want to add to the cart?"
                closeButton={true}
                text="Please click ok button to add this good into the cart"
                isOpen={isOpen}
                actions={
                    [
                        <Button key={1} backgroundColor="#b3382c" onClick={() => handleOkAdd(codeOfGoodForModal)}
                                className="btn btn-ok" text="Ok"/>,
                        <Button key={2} backgroundColor="#b3382c" onClick={handleCancel} className="btn btn-cancel"
                                text="Cancel"/>
                    ]
                }
                handleCancel={handleCancel}
            />
            }

            {!isGoodAdd &&
            <Modal
                header="Do you want to delete?"
                closeButton={true}
                text="Please click ok button to delete this good from the cart"
                isOpen={isOpen}
                actions={
                    [
                        <Button key={1} backgroundColor="#b3382c" onClick={() => handleOkDelete(codeOfGoodForModal)}
                                className="btn btn-ok" text="Ok"/>,
                        <Button key={2} backgroundColor="#b3382c" onClick={handleCancel} className="btn btn-cancel"
                                text="Cancel"/>
                    ]
                }
                handleCancel={handleCancel}
            />
            }
        </div>
    )
}

export default App;
