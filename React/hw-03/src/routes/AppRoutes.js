import React from 'react';
import CardsList from "../components/CardsList/CardsList";
import {Redirect, Route, Switch} from "react-router-dom";
import Favorites from "../pages/Favorites/Favorites";
import Cart from "../pages/Cart/Cart";
import PropTypes from "prop-types";

const AppRoutes = (props) => {
    const {isOpen, addToFavorite, openModalAdd, openModalDelete, goods} = props;

    return (
        <>
            <Switch>
                <Redirect exact from="/" to="/home"/>
                <Route exact
                       path="/home"
                       render={() => <CardsList addToFavorite={addToFavorite}
                                                openModalAdd={openModalAdd} openModalDelete={openModalDelete}
                                                cards={goods}/>}
                />
                <Route exact
                       path="/favorites"
                       render={() => <Favorites isOpen={isOpen} addToFavorite={addToFavorite}
                                                openModalAdd={openModalAdd} openModalDelete={openModalDelete}
                                                cards={goods}/>}
                />
                <Route exact
                       path="/cart"
                       render={() => <Cart isOpen={isOpen} addToFavorite={addToFavorite} openModalAdd={openModalAdd}
                                           openModalDelete={openModalDelete}
                                           cards={goods}/>}
                />

            </Switch>
        </>
    );
};

AppRoutes.propTypes = {
    isOpen: PropTypes.bool,
    addToFavorite: PropTypes.func,
    openModalAdd: PropTypes.func,
    openModalDelete: PropTypes.func,
    goods: PropTypes.array,
}

AppRoutes.defaultProps = {
    isOpen: false,
    addToFavorite: () => {},
    openModalAdd: () => {},
    openModalDelete: () => {},
    goods: [],
}


export default AppRoutes;