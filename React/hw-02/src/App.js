import React, {Component} from 'react';
import CardsList from "./components/CardsList/CardsList";
import './App.scss';
import Modal from "./components/Modal/Modal";
import Button from "./components/Button/Button";

class App extends Component {

    state = {
        goods: null,
        isLoaded: false,
        isOpen: false,
        codeOfGoodForModal: 0,
    }

    normalizedData = data => {
        return data.map(el => {
            const favorites = JSON.parse(localStorage.getItem("favorites")) || [];
            const inCart = JSON.parse(localStorage.getItem("inCart")) || [];
            el.isFavorite = favorites.includes(el.code);
            el.inCart = inCart.some(elem => elem.id === el.code);
            return el;
        })
    }

    componentDidMount() {
        fetch("goods.json").then(res => res.json()).then(data => {
            const normalizedGoods = this.normalizedData(data);
            this.setState({goods: normalizedGoods, isLoaded: true});
        })
    }

    setToLocalStorage = (code, storageName) => {
        let arr = JSON.parse(localStorage.getItem(storageName)) || [];

        if (storageName === "inCart") {
            if (arr.some(el => el.id === code)) {
                arr.forEach(el => {
                        if (el.id === code) {
                            el.count = el.count + 1
                        }
                    }
                )
            } else {
                arr.push({'id': code, 'count': 1})
            }
            const inCart = JSON.stringify(arr);
            localStorage.setItem(storageName, inCart);
        } else {
            arr = (arr.includes(code) ? arr.filter(el => el !== code) : arr.concat(code));
        }
        const favorites = JSON.stringify(arr);
        localStorage.setItem(storageName, favorites);
    }

    addToFavorite = code => {
        const {goods} = this.state;

        const newArr = goods.map(el => {
            if (el.code === code) {
                el.isFavorite = !el.isFavorite
            }
            return el;
        })
        this.setToLocalStorage(code, "favorites");
        this.setState({goods: newArr});
    }

    addToCart = code => {
        const {goods} = this.state;

        const newArr = goods.map(el => {
            if (el.code === code) {
                el.inCart = true;
            }
            return el;
        })
        this.setToLocalStorage(code, "inCart");
        this.setState({goods: newArr});
    }

    openModal = code => {
        this.setState({isOpen: true, codeOfGoodForModal: code});
    }

    handleOk = code => {
        this.setState({isOpen: false});
        this.addToCart(code);
    }

    handleCancel = () => {
        this.setState({isOpen: false});
    }


    render() {
        const {goods, isLoaded, isOpen, codeOfGoodForModal} = this.state;

        if (!isLoaded) {
            return <div>...Loading</div>;
        }

        return (
            <div className="App">
                <CardsList isOpen={isOpen} addToFavorite={this.addToFavorite} openModal={this.openModal} cards={goods}
                           handleOk={this.handleOk} handleCancel={this.handleCancel}/>
                <Modal
                    header="Do you want to add to the cart?"
                    closeButton={true}
                    text="Please click ok button to add this good into the cart"
                    isOpen={isOpen}
                    actions={
                        [
                            <Button key={1} backgroundColor="#b3382c" onClick={() => this.handleOk(codeOfGoodForModal)}
                                    className="btn btn-ok" text="Ok"/>,
                            <Button key={2} backgroundColor="#b3382c" onClick={this.handleCancel}
                                    className="btn btn-cancel" text="Cancel"/>
                        ]
                    }
                    handleCancel={this.handleCancel}
                />
            </div>
        )
    }
}

export default App;
