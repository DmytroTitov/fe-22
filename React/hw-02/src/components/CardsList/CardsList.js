import React, {Component} from 'react';
import Card from "../Card/Card";
import "./CardsList.scss"
import PropTypes from "prop-types";

class CardsList extends Component {
    render() {
        const {cards, isOpen, addToFavorite, openModal} = this.props;
        return (
            <div className="cards-container container">
                {cards.map(card => {
                    return <Card key={card.code} card={card} isOpen={isOpen} addToFavorite={addToFavorite} openModal={openModal} />
                })}
            </div>
        );
    }
}

CardsList.propTypes = {
    cards: PropTypes.array,
}

CardsList.defaultProps = {
    cards: [],
}

export default CardsList;